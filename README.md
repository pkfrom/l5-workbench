# Workbench

### Installation

1. Add this in composer.json

```
"fromz/workbench": "dev-master"
```

and

```
"repositories": [
	{
		"type": "vcs",
		"url": "git@bitbucket.org:pkfrom/l5-workbench.git"
	}
],
```

2. Run composer

```
composer update
```

3. Add the Service Provider

Open `config/app.php` and, to your **providers** array at the bottom, add:

```
Fromz\Workbench\WorkbenchServiceProvider::class,
```


### Creating A Package

> Before you create a package, you need to update `name` and `email` config value in your `config/workbench.php` file. 

Creating a basic package.

```
php artisan workbench vendor/package
```

Creating a package with generating some scaffold resources.

```
php artisan workbench vendor/package --resources
```

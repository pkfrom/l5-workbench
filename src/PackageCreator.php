<?php

namespace Fromz\Workbench;

use Illuminate\Filesystem\Filesystem;

class PackageCreator
{
    protected $files;

    protected $basicBlocks = array(
        'SupportFiles',
        'TestDirectory',
        'ServiceProvider',
    );

    protected $blocks = array(
        'SupportFiles',
        'SupportDirectories',
        //'PublicDirectory',
        'TestDirectory',
        'ServiceProvider',
        'ConfigFile',
        'RoutesFile',
    );

    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }

    public function create(Package $package, $path, $plain = true)
    {
        $directory = $this->createDirectory($package, $path);

        foreach ($this->getBlocks($plain) as $block) {
            $this->{"write{$block}"}($package, $directory, $plain);
        }

        return $directory;
    }

    public function createWithResources(Package $package, $path)
    {
        return $this->create($package, $path, false);
    }

    protected function getBlocks($plain)
    {
        return $plain ? $this->basicBlocks : $this->blocks;
    }

    public function writeSupportFiles(Package $package, $directory, $plain)
    {
        foreach (array('PhpUnit', 'Travis', 'Composer', 'Ignore', 'Readme', 'License') as $file) {
            $this->{"write{$file}File"}($package, $directory, $plain);
        }
    }

    protected function writePhpUnitFile(Package $package, $directory)
    {
        $stub = __DIR__.'/stubs/phpunit.xml';

        $this->files->copy($stub, $directory.'/phpunit.xml');
    }

    protected function writeRoutesFile(Package $package, $directory)
    {
        $stub = __DIR__.'/stubs/routes.php';

        $this->files->copy($stub, $directory.'/src/routes.php');
    }

    protected function writeTravisFile(Package $package, $directory)
    {
        $stub = __DIR__.'/stubs/.travis.yml';

        $this->files->copy($stub, $directory.'/.travis.yml');
    }

    protected function writeComposerFile(Package $package, $directory, $plain)
    {
        $stub = $this->getComposerStub($plain);

        $stub = $this->formatPackageStub($package, $stub);

        $this->files->put($directory.'/composer.json', $stub);
    }

    protected function writeReadmeFile(Package $package, $directory)
    {
        $stub = $this->files->get(__DIR__.'/stubs/README.md');

        $stub = $this->formatPackageStub($package, $stub);

        $this->files->put($directory.'/README.md', $stub);
    }

    protected function writeLicenseFile(Package $package, $directory)
    {
        $stub = $this->files->get(__DIR__.'/stubs/LICENSE.stub');

        $stub = $this->formatPackageStub($package, $stub);

        $this->files->put($directory.'/LICENSE', $stub);
    }

    protected function getComposerStub($plain)
    {
        if ($plain) {
            return $this->files->get(__DIR__.'/stubs/plain.composer.json');
        }

        return $this->files->get(__DIR__.'/stubs/composer.json');
    }

    public function writeIgnoreFile(Package $package, $directory, $plain)
    {
        $this->files->copy(__DIR__.'/stubs/gitignore.txt', $directory.'/.gitignore');
    }

    public function writeSupportDirectories(Package $package, $directory)
    {
        foreach (array('config', 'resources/lang', 'migrations', 'resources/views') as $support) {
            $this->writeSupportDirectory($package, $support, $directory);
        }
    }

    protected function writeSupportDirectory(Package $package, $support, $directory)
    {
        $path = $directory.'/src/'.$support;

        $this->files->makeDirectory($path, 0777, true);

        $this->files->put($path.'/.gitkeep', '');
    }

    public function writePublicDirectory(Package $package, $directory, $plain)
    {
        if ($plain) {
            return;
        }

        $this->files->makeDirectory($directory.'/public');

        $this->files->put($directory.'/public/.gitkeep', '');
    }

    public function writeConfigFile(Package $package, $directory, $plain)
    {
        $this->files->copy(__DIR__.'/stubs/config.php', $directory.'/src/config/config.php');
    }

    public function writeTestDirectory(Package $package, $directory)
    {
        $this->files->makeDirectory($directory.'/tests');

        $this->files->put($directory.'/tests/.gitkeep', '');
    }

    public function writeServiceProvider(Package $package, $directory, $plain)
    {
        $stub = $this->getProviderStub($package, $plain);

        $this->writeProviderStub($package, $directory, $stub);
    }

    protected function writeProviderStub(Package $package, $directory, $stub)
    {
        $path = $this->createClassDirectory($package, $directory);

        $file = $path.'/'.$package->name.'ServiceProvider.php';

        $this->files->put($file, $stub);
    }

    protected function getProviderStub(Package $package, $plain)
    {
        return $this->formatPackageStub($package, $this->getProviderFile($plain));
    }

    protected function getProviderFile($plain)
    {
        if ($plain) {
            return $this->files->get(__DIR__.'/stubs/plain.provider.stub');
        }

        return $this->files->get(__DIR__.'/stubs/provider.stub');
    }

    protected function createClassDirectory(Package $package, $directory)
    {
        $path = $directory.'/src/'.$package->name;

        if (!$this->files->isDirectory($path)) {
            $this->files->makeDirectory($path, 0777, true);
        }

        return $path;
    }

    protected function formatPackageStub(Package $package, $stub)
    {
        foreach (get_object_vars($package) as $key => $value) {
            $stub = str_replace('{{'.snake_case($key).'}}', $value, $stub);
        }

        return $stub;
    }

    protected function createDirectory(Package $package, $path)
    {
        $fullPath = $path.'/'.$package->getFullName();

        if (!$this->files->isDirectory($fullPath)) {
            $this->files->makeDirectory($fullPath, 0777, true);

            return $fullPath;
        }

        throw new \InvalidArgumentException('Package exists.');
    }
}

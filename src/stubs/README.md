# {{name}}

### Installation

1. Add this in composer.json

```
"{{lower_vendor}}/{{lower_name}}": "dev-master"
```

and

```
"repositories": [
	{
		"type": "vcs",
		"url": "git@github.com:{{lower_vendor}}/{{lower_name}}.git"
	}
],
```

2. Run composer

```
composer update
```

3. Add the Service Provider

Open `config/app.php` and, to your **providers** array at the bottom, add:

```
{{vendor}}\{{name}}\{{name}}ServiceProvider::class,
```